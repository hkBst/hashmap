#include <memory>
#include <functional>
#include <atomic>
#include <iostream>

using namespace std;

//const auto mem_order = memory_order_seq_cst;
const auto mem_order = memory_order_relaxed;

/*
  A thread-safe (I think wait-free) hashmap using linear probing that
  supports only lookup, insert and erase.

  Initially the hashmap contains only (shared) null_ptr's, into which
  any key-value pair can be atomically inserted.  To enable the erase
  operation, key-value pairs store the value indirectly via another
  shared_ptr, which can be nulled to represent an erased value.  An
  erased key-value pair can only be reused by re-inserting a key-value
  pair with an equivalent key, so erased but not re-inserted pairs
  take up space and slow down operations for other keys.

  Does not support resizing.
 */
template <class Key, class Val, class Hash = hash<Key> >
class hashmap {
  typedef pair<const Key, shared_ptr<const Val> > KeyVal;

  template <class K, class V>
  friend ostream& operator<<(ostream& os, const hashmap<K,V>& h);

private:
  const unique_ptr<shared_ptr<KeyVal>[]> m_table;
  const size_t m_size;
  const Hash m_hash;

public:
  explicit hashmap(size_t sz, Hash h = Hash())
    : m_table(new shared_ptr<KeyVal>[sz]), m_size(sz), m_hash(h) {}

  shared_ptr<const Val> lookup (const Key& key) const {
    const auto start_idx = m_hash(key) % m_size;
    auto idx = start_idx;
    do {
      auto p = atomic_load_explicit(m_table.get()+idx, mem_order);
      if ( !p ) return 0;
      if ( p->first == key ) // keys never change: non-atomic load ok
        return atomic_load_explicit(&(p->second), mem_order);
    } while ( idx = (idx + 1) % m_size, idx != start_idx );
    return 0;
  }
  
  void erase(const Key& key) {
    const auto start_idx = m_hash(key) % m_size;
    auto idx = start_idx;
    do {
      auto p = atomic_load_explicit(m_table.get()+idx, mem_order);
      if ( !p ) return; // not found
      else if ( p->first == key ) // keys never change: non-atomic load ok
        return atomic_store_explicit(&(p->second), shared_ptr<const Val>(), mem_order);
    } while ( idx = (idx + 1) % m_size, idx != start_idx );
  }
  
  bool insert(const Key& key, const Val& val) {
    const auto start_idx = m_hash(key) % m_size;
    auto idx = start_idx;
    auto kvp = make_shared<KeyVal>(key, make_shared<const Val>(val));
    do {
      auto p = atomic_load_explicit(m_table.get()+idx, mem_order);
      if ( (!p) &&
           atomic_compare_exchange_strong_explicit
           (m_table.get() + idx, &p, kvp, mem_order, mem_order) )
        return true;
      // if p already points to something,
      // or if someone else just inserted something before us, (causing p to be reloaded),
      // then we continue to examine the key
      if ( p->first == key ) { // keys never change: non-atomic load ok
        auto pv = atomic_load_explicit(&(p->second), mem_order);
        // if this key is erased, then either we insert it
        // or someone else just inserted it (and we do not set it)
        return (!pv) &&
          atomic_compare_exchange_strong_explicit
          (&(p->second), &pv, make_shared<const Val>(val), mem_order, mem_order);
      }
    } while ( idx = (idx + 1) % m_size, idx != start_idx );
    return false;
  }
};

template <class K, class V>
ostream& operator<<(ostream& os, const hashmap<K,V>& h) {
  for (size_t i = 0; i < h.m_size; ++i) {
    os << i << ": ";
    auto p = atomic_load_explicit(h.m_table.get()+i, mem_order);
    if ( !p ) { os << "empty" << endl; continue; }
    auto pv = atomic_load_explicit(&(p->second), mem_order);
    if ( !pv ) { os << "erased" << endl; continue; }
    // keys never change: non-atomic load ok
    os << "key: `" << p->first << "', value: `" << *pv << "'" << endl;
  }
  return os;
}
