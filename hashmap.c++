#include "hashmap.h++"
#include <iostream>
#include <thread>
#include <sstream>
#include <vector>

using namespace std;

void insert_key(hashmap<string,string>& h, int n) {
  stringstream k; k << "key" << n;
  stringstream v; v << "value" << n;
  h.insert(k.str(), v.str());
}

void erase_key(hashmap<string,string>& h, int n) {
  stringstream k; k << "key" << n;
  h.erase(k.str());
}

void lookup_key(hashmap<string,string>& h, int n) {
  stringstream k; k << "key" << n;
  auto p = h.lookup(k.str());
  if ( p )
    cout << "Found key `" << k.str() << "' with value `" << *p << "'" << endl;
  else
    cout << "Did not find any value for key `" << k.str() << "'" << endl;
}

void insert_erase(hashmap<string,string>& h, int n) {
  const int n_iterations = 10000;
  
  for (int i=0; i<n_iterations; ++i) {
    stringstream k; k << "key" << n;
    stringstream v; v << "value" << n << "_" << i;
    h.insert(k.str(), v.str());
    h.erase(k.str());
    h.insert(k.str(), v.str());
  }
}

// number of keys used, usually one thread per key
const int n_keys = 10;
// number of threads using each key (only for main test)
const int key_concurrency = 10;
const size_t hashmap_size = 20;

int main() {

  hashmap<string, string> hm(hashmap_size);
  
  vector<thread> vt;

  cout << "Inserting keys...\n" << endl;

  // insert keys
  for (int i = 0; i < n_keys; ++i)
    vt.push_back(move(thread(insert_key, ref(hm), i)));
 
  // wait until all threads have finished running and empty the vector
  for (auto& t: vt) t.join();
  vt.clear();

  cout << "Looking keys up...\n" << endl;
  for (int i = 0; i < n_keys; ++i)
    vt.push_back(move(thread(lookup_key, ref(hm), i)));
  for (auto& t: vt) t.join();
  vt.clear();

  cout << "\nErasing keys...\n" << endl;
  for (int i = 0; i < n_keys; ++i)
    vt.push_back(move(thread(erase_key, ref(hm), i)));
  for (auto& t: vt) t.join();
  vt.clear();

  cout << "Looking keys up...\n" << endl;
  for (int i = 0; i < n_keys; ++i)
    vt.push_back(move(thread(lookup_key, ref(hm), i)));
  for (auto& t: vt) t.join();
  vt.clear();

  cout << "\nInserting, erasing and looking keys up... (concurrently)\n" << endl;
  for (int i = 0; i < n_keys; ++i) {
    for (int j = 0; j < key_concurrency; ++j)
      vt.push_back(move(thread(insert_erase, ref(hm), i)));
  }
  // no waiting yet
  for (int i = 0; i < n_keys; ++i)
    vt.push_back(move(thread(lookup_key, ref(hm), i)));
  
  // wait until all threads have finished running
  for (auto& t: vt) t.join();
  vt.clear();

  cout << "\nTesting final state...\n" << endl;
  
  // test final hashmap state
  for (int i = 0; i < n_keys; ++i)
    vt.push_back(move(thread(lookup_key, ref(hm), i)));
  for (auto& t: vt) t.join();

  cout << "\nHashmap data dump:\n" << endl;
  cout << hm << endl;
}
