#CXXFLAGS = -std=c++20 -ggdb -Wall -O2 -fsanitize=thread -pthread
CXXFLAGS = -std=c++20 -Wall -O2 -pthread

ALL = recursion hashmap

all: $(ALL)

hashmap: hashmap.h++

%: %.c++ Makefile
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -o $@

clean:
	$(RM) $(ALL)

.PHONY: all clean
